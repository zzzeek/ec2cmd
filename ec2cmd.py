import boto.ec2
import time
import subprocess
import os
import argparse

def log(message, *args):
    print message % args


def ssh_cmd(username, host, cmd, ssh_key=None):
    cmd_args = ["ssh", "-o", "StrictHostKeyChecking=no"]
    if ssh_key:
        cmd_args.extend(["-i", ssh_key])
    cmd_args.append("%s@%s" % (username, host))
    cmd_args.append(cmd)

    p = subprocess.Popen(cmd_args)
    return p.wait()

def start_instance(conn, instance_id, username, ssh_key):
    ssh_opts = {
        "ssh_key": ssh_key
    }

    log("Starting instance %s", instance_id)
    instances = conn.start_instances(instance_id)
    instance = instances[0]

    instance.update()
    while instance.state == 'pending':
        log("waiting for instance to come up")
        time.sleep(10)
        instance.update()

    host = instance.dns_name

    for i in range(10):
        returncode = ssh_cmd(username, host,
                        "echo 'ssh connection successful'", **ssh_opts)
        if returncode == 0:
            return instance
        log("Waiting for %s to come up, sleeping", host)
        time.sleep(5)
    else:
        log("Host didn't come up for 60 seconds, quitting")
        return None


def run_cmd(instance, username, ssh_key, cmd):
    host = instance.dns_name
    return ssh_cmd(username, host, cmd, ssh_key=ssh_key)


def main(argv=None):
    parser = argparse.ArgumentParser(prog="ec2cmd")

    parser.add_argument("-i", "--instance", type=str, help="Instance id")
    #parser.add_argument("--ami", type=str, help="AMI id")
    parser.add_argument("-u", "--username", type=str, help="ec2 username",
                                default='ec2-user')
    parser.add_argument("-a", "--availability", type=str,
                                help="availability zone")
    parser.add_argument("-k", "--key", type=str, help="Filepath to ssh key")
    parser.add_argument("cmd", type=str, help="command to run")

    args = parser.parse_args(argv)

    conn = boto.ec2.connect_to_region(args.availability)
    instance = start_instance(conn, args.instance, args.username, args.key)
    if instance is not None:
        run_cmd(instance, args.username, args.key, args.cmd)
        instance.stop()

