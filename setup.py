from setuptools import setup


setup(name='ec2cmd',
      version=1.0,
      description="Launch EC2 instances and run commands on them",
      classifiers=[
      'Development Status :: 4 - Beta',
      'Environment :: Console',
      'Programming Language :: Python',
      'Programming Language :: Python :: 3',
      'Programming Language :: Python :: Implementation :: CPython',
      'Programming Language :: Python :: Implementation :: PyPy',
      ],
      author='Mike Bayer',
      author_email='mike@zzzcomputing.com',
      url='http://bitbucket.org/zzzeek/ec2cmd',
      license='MIT',
      py_modules=["ec2cmd"],
      zip_safe=False,
      entry_points={
        'console_scripts': ['ec2cmd = ec2cmd:main'],
      }
)
